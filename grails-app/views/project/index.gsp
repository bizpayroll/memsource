
<%@ page import="com.example.memsource.Configuration" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <title>Projects</title>
</head>
<body>
<div class="nav" role="navigation">
    <ul>
        <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
    </ul>
</div>
<div id="listProjects" class="content scaffold-show" role="main">
    %{--<g:render template="list" model="[projects: projects]"/>--}%
</div>
<div>
    <label>Enter Page Number</label>
    <input type="text" id="pageNumber">
    <button onclick="getProjects()">Projects</button>
</div>
<script>
    function getProjects() {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById("listProjects").innerHTML = xhttp.responseText;
            }
        };
        xhttp.open("GET", "${g.createLink(controller: 'project', action: 'paginateProjects')}?page="+document.getElementById("pageNumber").value, true);
        xhttp.send();
    }
</script>
</body>
</html>
