<g:if test="${projects}">
<table>
    <tr>
        <th>Name</th>
        <th>Status</th>
        <th>Source Language</th>
        <th>Target Language</th>
    </tr>
    <g:each in="${projects}" var="project">
        <tr>
            <td>${project.name}</td>
            <td>${project.status}</td>
            <td>${project.sourceLang}</td>
            <td>${project.targetLangs?.join(",")}</td>
        </tr>
    </g:each>
</table>
</g:if>
<g:else>
    <b>No Projects Found</b>
</g:else>