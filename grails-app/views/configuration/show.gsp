
<%@ page import="com.example.memsource.Configuration" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'configuration.label', default: 'Configuration')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-configuration" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="show-configuration" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list configuration">
			
				<g:if test="${configuration?.username}">
				<li class="fieldcontain">
					<span id="username-label" class="property-label"><g:message code="configuration.username.label" default="Username" /></span>
					
						<span class="property-value" aria-labelledby="username-label"><g:fieldValue bean="${configuration}" field="username"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${configuration?.password}">
				<li class="fieldcontain">
					<span id="password-label" class="property-label"><g:message code="configuration.password.label" default="Password" /></span>
					
						<span class="property-value" aria-labelledby="password-label"><g:fieldValue bean="${configuration}" field="password"/></span>
					
				</li>
				</g:if>
				<g:if test="${!configuration?.id}">
					<g:link class="create" action="edit">New</g:link>
				</g:if>
				<g:else>
					<g:link class="create" action="edit">Edit</g:link>
				</g:else>
			
			</ol>

		</div>
	</body>
</html>
