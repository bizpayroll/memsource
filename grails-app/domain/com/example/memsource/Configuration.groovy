package com.example.memsource

/*Authentication details for external rest api call*/
class Configuration {

    /*External Username for memsource api authentication*/
    String username

    /*Password for above user*/
    String password

    /*Token*/
    String token

    /*Token Expiry Date*/
    Date expiryDate


    static constraints = {
        username(nullable: false, blank: false)
        password(nullable: false, blank: false)
        token(nullable: true)
        expiryDate(nullable: true, validator: {val, obj ->
            if(obj.token && !obj.expiryDate){
                return "expiry.date.cannot.be.null"
            }
        })
    }
}
