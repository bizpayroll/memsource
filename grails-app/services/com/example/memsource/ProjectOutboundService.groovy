package com.example.memsource

import grails.transaction.Transactional
import org.apache.commons.httpclient.methods.PostMethod
import org.codehaus.groovy.grails.web.json.JSONObject
import org.codehaus.groovy.grails.web.json.JSONArray
import org.apache.commons.httpclient.HttpClient

import java.text.SimpleDateFormat

@Transactional
class ProjectOutboundService {

    static final String BASE_URL = "http://cloud.memsource.com/web"
    static final String LOGIN_URL = "api/v3/auth/login"
    static final String PROJECTS_URL = "api/v3/project/list"

    Date parseJsonDate(String dateString){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date d = sdf.parse(dateString);
        return d
    }

    Configuration authentication(Configuration configuration){
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = null
        String url = "${BASE_URL}/${LOGIN_URL}"
        println "request ${url}"
        postMethod = new PostMethod(url);
        postMethod.addParameter("userName", configuration.username)
        postMethod.addParameter("password", configuration.password)
        int sc = httpClient.executeMethod(postMethod);
        String responseBody = postMethod.getResponseBodyAsString();
        println "response from SF at the time of authenticate  ${responseBody}"
        JSONObject json = new JSONObject(responseBody?:"{}");
        configuration.token = json.get("token")
        configuration.expiryDate = parseJsonDate(json.get("expires")?.toString())
        configuration.save(flush: true)
        return configuration
    }

    JSONArray retrieveProjects(Configuration configuration, Integer page = 0){
        if(new Date() >= configuration?.expiryDate){
            configuration = authentication(configuration)
        }
        HttpClient httpClient = new HttpClient();
        PostMethod postMethod = null
        String url = "${BASE_URL}/${PROJECTS_URL}"
        println "request ${url}"
        postMethod = new PostMethod(url);
        postMethod.addParameter("token", configuration.token)
        postMethod.addParameter("page", page?.toString())
        int sc = httpClient.executeMethod(postMethod);
        String responseBody = postMethod.getResponseBodyAsString();
        println "response from SF at the time of fetch projects  ${responseBody}"
        responseBody = "{projects:${responseBody}}"
        println "response from SF at the time of fetch projects  ${responseBody}"
        JSONObject json = new JSONObject(responseBody?:"{}");
        JSONArray jsonArray = json.getJSONArray("projects")
        println "jsonArray size ${jsonArray.length()}"

        return jsonArray
    }
}
