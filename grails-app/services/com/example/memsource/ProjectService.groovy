package com.example.memsource

import grails.transaction.Transactional
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

@Transactional
class ProjectService {

    def configurationService
    def projectOutboundService

    List<Map> fetchProjects(Configuration configuration, Integer page){
        JSONArray jsonArray = projectOutboundService.retrieveProjects(configuration, page)
        List projects = []
        for(int i =0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i)
            Map map = [:]
            map.id = jsonObject.get("id")
            map.name = jsonObject.get("name")
            map.status = jsonObject.get("status")
            map.sourceLang = jsonObject.get("sourceLang")
            map.targetLangs = jsonObject.get("targetLangs")
            projects << map
        }
        println "projects  ${projects}"
        return projects
    }
}
