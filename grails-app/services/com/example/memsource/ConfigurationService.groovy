package com.example.memsource

import grails.transaction.Transactional

@Transactional
class ConfigurationService {

    Configuration fetchConfiguration(){
        Configuration configuration = Configuration.createCriteria().get {
            isNotNull("id")
            isNotNull("username") // For safe side
        }
        return configuration
    }
}
