class UrlMappings {

	static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/configuration/index"(controller: "configuration", action: "show"){

        }

        "/"(view:"/index")
        "500"(view:'/error')
	}
}
