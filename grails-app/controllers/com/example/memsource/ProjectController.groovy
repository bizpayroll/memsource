package com.example.memsource

class ProjectController {

    def configurationService
    def projectService

    def index() {
        Configuration configuration = configurationService.fetchConfiguration()
        if(!configuration){
            flash.errorMessage = "Please configure username and password first"
            redirect(controller: "configuration", action: "edit")
            return
        }
        return [projects: []]
    }

    def paginateProjects(){
        Integer page = 0
        if(params.page){
            page = Integer.parseInt(params?.page?.toString())
        }
        Configuration configuration = configurationService.fetchConfiguration()
        if(!configuration){
            flash.errorMessage = "Please configure username and password first"
            redirect(controller: "configuration", action: "edit")
            return
        }
        List<Map>  projects = projectService.fetchProjects(configuration, page)
        render(template: "list", model: [projects: projects])
    }
}
