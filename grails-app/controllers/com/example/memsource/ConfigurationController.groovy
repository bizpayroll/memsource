package com.example.memsource


import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class ConfigurationController {

    def configurationService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        redirect(action: "show")
    }

    def show() {
        Configuration configuration = configurationService.fetchConfiguration()
        if(!configuration){
            configuration = new Configuration()
        }
        return [configuration: configuration]
    }

    @Transactional
    def save() {
        Configuration configuration = configurationService.fetchConfiguration()
        if(!configuration){
            configuration = new Configuration()
        }
        configuration.username = params.username
        configuration.password = params.password
        if(configuration.validate() && !configuration.hasErrors()){
            configuration.save(flush: true)
            redirect(action: "show")
        }else{
            render(view: "edit", model: [configuration: configuration])
        }
    }

    def edit() {
        Configuration configuration = configurationService.fetchConfiguration()
        if(!configuration){
            configuration = new Configuration()
        }
        return [configuration: configuration]
    }
}
